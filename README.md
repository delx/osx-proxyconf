# OSX ProxyConf

Set environment variables based on OSX proxy settings from system preferences.

## Introduction

Do you use Mac OSX's multiple network locations feature with different proxies? This is a great way to conveniently use applications in multiple locations without having to configure each app separately.

Now you've probably noticed that command line applications do not use the proxy settings from System Preferences. This includes things like `wget`, `brew`, etc These applications all actually pay attention to the Unix convention of putting proxies in environment variables, eg
```
$ export http_proxy="http://www-cache.example.com"
```

Proxyconf allows you to configure your proxies in System Preferences, and it takes care of grabbing these values and exporting them as environment variables. The upshot of this is, your command line apps will use the same proxy settings as your GUI apps.


## Installation from binary

The latest binary is: [proxyconf-20090208.tar.gz](https://delx.net.au/projects/osx-proxyconf/proxyconf-20090208.tar.gz).


## Configuration

Now add the following line to your `~/.bashrc` (modify as appropriate):
```
eval $(~/Downloads/proxyconf/proxyconf)
```

## Compile from source

Installation is easy. Note the following assumes you've already configured your proxies in System Preferences. Skip to the last step if you downloaded a binary version.

Firstly, grab a copy of the code using Git:
```
$ cd ~/Downloads/
$ git clone https://delx.net.au/git/proxyconf
```

Next compile it:
```
$ cd proxyconf
$ make
```


## Limitations

Note that proxyconf only works if you have System Preferences set to configure
proxies "Manually". For example, proxy.pac files are not supported.

The environment variables are only exported when the shell starts. This means
if you have a terminal open and change location the environment will be
incorrect. This is unavoidable. The workaround is to close and start the
terminal, or to run `exec bash -i`

## Issues

Please file issues at the [Bitbucket project](https://bitbucket.org/delx/osx-proxyconf).
