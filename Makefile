CFLAGS     := -Wall -W -std=c99 -arch i386 -arch ppc $(CFLAGS)
LDFLAGS    := $(LDFLAGS)
FRAMEWORKS := -framework CoreFoundation -framework SystemConfiguration -framework Foundation
TARGETS    := proxyconf

all: $(TARGETS)

%: %.m
	export MACOSX_DEPLOYMENT_TARGET=10.4; \
	$(CC) -o $@ $(CFLAGS) $(LDFLAGS) $(FRAMEWORKS) $<

clean:
	rm -f $(TARGETS)

.PHONY: clean
